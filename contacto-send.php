<?php 
/**
 * Template Name: Contacto Send
 *
 */
?>
<?php get_header(); ?>

<?php
$nombre = $_POST['nombre_usuario'];
$email = $_POST['email'];
$tlf = $_POST['telefono'];
$localidad = $_POST['localidad'];
$mensaje = $_POST['comentarios'];
;

if ($nombre!="" and $email!="" and $localidad!="") {
    $json = json_decode(str_replace('\\', '', $adicionales), true);
    $json_grupos = json_decode(str_replace('\\', '', $adicionales_grupos), true);
    /// ADMINISTRADOR ///////
///
///
///
    $emailTo = 'ventas@poparq.com.ar';
    //$emailTo = 'dgmarcosmoreira@gmail.com';


    $subject = 'Mensaje desde el sitio: ' . $nombre . ' - '. $email;
    $body = '';
    $body .= "Nombre: " . $nombre . "\n\n\n";
    $body .= "Telefono: " . $tlf . "\n\n\n";
    $body .= "Email: " . $email . "\n\n\n";
    $body .= "Localidad: " . $localidad . "\n\n\n";
    $body .= "Comentarios: " . $mensaje . "\n\n\n";
    $body .= "Adicionales seleccionados: \n\n\n\n";

    $headers = 'From: ' . $name . ' <' . $emailTo . '>' . "\r\n" . 'Reply-To: ' . $email;
    wp_mail($emailTo, $subject, $body, $headers);
    $emailSent = true;


    ///// USUARIO /////

    if (isset($emailSent) && $emailSent == true) {
        $return = 'ok';

    } else {
        $return = 'no';
    }
}else{
   $return = '¿Haciendo trampa?';
}
//json_encode($return);

?>
<section id="main" class="relative main single">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-8 text-center">
                <?php if($return == 'ok'){ ?>
                    <div class="supericon ok">
                        <i class="far fa-check-circle"></i>
                    </div>
                <h1>¡Genial!</h1>
                <div class="line-block">
                    <h4 class="lined">Tu correo ha sido enviado exitosamente. Te responderemos a la brevedad</h4>
                </div>
    <?php }elseif ($return == 'no'){ ?>
                    <h1>¡Lo sentimos!</h1>
                    <div class="line-block">
                        <h4 class="lined">Tuvimos un problema al enviar tu información.</h4>
                    </div>
               <?php  }else{ ?>
                    <h1>Parece que hubo algún problema....</h1>
                    <div class="line-block">
                        <h4 class="lined"><small>Intenta nuevamente más tarde</small></h4>
                    </div>
                <?php } ?>
            </div>
            <div class="my-4 col-12"></div>
            <div class="col-12 text-center">
                <a href="<?php echo home_url() ?>/catalogo" class="btn btn-primary btn-lg ">VOLVER AL CATÁLOGO</a>
            </div>
            <div class="col-12 mt-3 text-center">
                <a href="<?php echo home_url() ?>" class="btn btn-link">IR AL INICIO</a>
            </div>
        </div>
    </div>
    <div class="spacer-1"></div>
</section>
<?php get_footer(); ?>
  <?php /* Template Name: Catalogo */ ?>
  <?php include ('header.php'); ?>

  <section id="main" class="mb-4 full-height relative">
      <div class="gradient"></div>
      <div class="image-cover" style="background-image:url('<?php echo get_template_directory_uri() ?>/assets/img/catalogo/catalogo.jpg')"></div>
        <div class="container">
          <div class="row align-items-center full-height">
              <div class="col-lg-6 texto-backdrop">
                <h1 class="text-white">Mayor control </br>
                menor inversión</h1>
                <p class="text-white lead">Optimizamos el proceso de contrucción tradicional para que tengas tu casa en el menor tiempo, con los mejores materiales y al menor costo.</p>
                <p><a class="btn btn-warning" href="#" role="button">ELEGÍ TU MODELO</a></p>
              </div>
          </div>
        </div>
  </section>

  <?php include('inc/section-personalizar.php'); ?>

        <section class="section" id="catalogo">
        <div class="container">
          <div class="row">
            <div class="col-md-6 text-center text-md-left">
              <h4>Modelos disponibles</h4>
            </div>
            <!--<div class="col-md-6 text-center text-md-left">
              <select class="float-md-right ml-2 form-control">
                <option value="0">
                  Precio
                </option>
              </select>
              <select class="float-md-right form-control d-inline">
                <option value="0">
                  Cantidad de ambientes
                </option>
              </select>
            </div>-->
          </div>
          <div class="row">
            <?php
                $args = array(
                'post_type'      => 'modelo',
                'posts_per_page' => -1,
                    'post_parent' => 0,
                'order'          => 'ASC',
                'orderby'        => 'menu_order'
                );
             ?>
            <?php query_posts($args); ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <?php include('inc/card-product.php'); ?>
            <?php endwhile; ?>
        </div>
          <div class="spacer-2"></div>
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <p class="lead">
                        ¿Ninguno se adapta a lo que necesitás? ¡Consultanos por modelos personalizados!
                    </p>
                    <p>
                        <a href="<?php echo home_url(); ?>/contacto" class="btn btn-outline-primary btn-block">CONSULTANOS</a>
                    </p>
                </div>
            </div>
        </div>
      </section>

<?php include ('footer.php'); ?>
<?php get_header() ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<section class="main">
    <?php if (is_mobile()){ ?>
        <?php the_post_thumbnail('medium', array('class' => 'img-fluid d-block d-lg-none')); ?>
    <?php } ?>
    <div class="spacer-2"></div>

    <div class="container">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="<?php if (has_post_thumbnail()){ echo 'col-lg-6'; }else { echo 'col-lg-8'; } ?>">
                <p class="text-uppercase"><small><?php the_date(); ?></small></p>
                <h1><?php the_title(); ?></h1>
                <?php if ( 'historias' == get_post_type() ){ ?>
                   <a href="<?php echo get_the_permalink($rel[0]->ID); ?>">
                       <h3><small><?php $rel = get_field('modelo'); echo get_the_title($rel[0]->ID); ?> | <?php the_field('ambientes', $rel[0]->ID); ?> ambientes</small></h3>
                   </a>
                <?php } ?>
                <?php if (get_field('resumen')){ ?>
                    <p class="lead"><?php the_field('resumen'); ?></p>
                <?php } ?>
            </div>
            <?php if (has_post_thumbnail()){ ?>
                <div class="col-lg-6">
                    <div class="single-image mid-height relative d-none d-lg-block">
                        <?php
                            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
                        ?>
                        <div class="image-cover" class="" style="background-image: url('<?php echo $featured_img_url; ?>');"></div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="spacer-1 d-block d-lg-none"></div>
         <?php if (has_post_thumbnail()){ ?>
            <div class="spacer-2 d-none d-lg-block"></div>
            <div class="spacer-2 d-none d-lg-block"></div>
        <?php } ?>
        <div class="row justify-content-center">
            <div class="col-lg-8" id="single-content">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
    <div class="spacer-2"></div>
</section>
<?php endwhile; endif; ?>
<?php get_footer() ?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/plugins/owl.carousel.min.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/main.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600|Poppins:300,300i,400,400i,500,500i,600,600i,700" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/animate.css@3.5.2/animate.min.css">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>
        <script id="__bs_script__">//<![CDATA[
            document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.24.5'><\/script>".replace("HOST", location.hostname));
            //]]></script>
        <?php wp_head(); ?>
    </head>

        <body>
            <!--[if lt IE 8]>
                <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
            <![endif]-->
            <!--<div class="loading"></div>-->
            <header>
              <nav class="navbar navbar-expand-lg fixed-top py-3 py-lg-2 navbar-dark bg-primary main-navbar">
                <div class="container">
                  <a href="<?php echo home_url() ?>"> <img id="logo" src="<?php echo get_template_directory_uri() ?>/assets/img/logo.svg" alt="Poparq - Arquitectura Optimizada"></a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse justify-content-center" id="navbarText">
                    <ul class="navbar-nav mr-auto">
                      <li class="nav-item">
                        <a class="nav-link" href="<?php echo home_url(); ?>/catalogo">CATÁLOGO</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="<?php echo home_url(); ?>/arquitectura-optimizada">ARQUITECTURA OPTIMIZADA</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="<?php echo home_url(); ?>/estudio">ESTUDIO</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="<?php echo home_url() ?>/blog">BLOG</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="<?php echo home_url() ?>/contacto">CONTACTO</a>
                      </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo home_url() ?>/mi-cuenta"><i class="fas fa-user-circle"></i> MI CUENTA</a>
                        </li>
                    </ul>
                  </div>
                </div>
              </nav>
            </header>

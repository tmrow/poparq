<?php /* Template Name: Historias */ ?>

<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
    <div class="spacer-2"></div>

  <section id="main" class="main relative">
      <div class="container">
          <div class="row justify-content-center">
              <div class="col-lg-6">
                  <div class="line-block">
                      <h2 class="lined"><?php the_title(); ?></h2>
                      <div class="lead">
                          <?php the_content() ?>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
<?php endwhile; ?>
<?php $cols='col-lg-4'; ?>
<div class="spacer-2"></div>
<section id="posts" class="relative">
    <div class="container">
        <div class="row">
            <?php
            $args = array(
                'post_type'      => 'historias',
                'posts_per_page' => 6,
                'order'          => 'ASC',
                'orderby'        => 'menu_order'
            );
            ?>
            <?php query_posts($args); ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <?php set_query_var( 'cols', $cols ); ?>
                <?php get_template_part('inc/card','historias'); ?>
            <?php endwhile; ?>
        </div>
        <?php echo do_shortcode('[ajax_load_more post_type="historias" posts_per_page="3" offset="6" progress_bar="true" progress_bar_color="483A7A" images_loaded="true"]'); ?>
    </div>
</section>
<div class="spacer-2"></div>
<div class="container">
<div class="row justify-content-center">
    <div class="col-lg-6 text-center">
        <p class="lead">
            ¡Queremos que la próxima sea tu historia!
        </p>
        <p>
            <a href="<?php echo home_url(); ?>/contacto" class="btn btn-outline-primary ">CONSULTANOS</a>
        </p>
    </div>
</div>
</div>
<div class="spacer-2"></div>


<?php get_footer(); ?>


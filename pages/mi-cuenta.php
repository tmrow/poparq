<?php /* Template Name: Mi cuenta */ ?>

<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
    <div class="spacer-2"></div>

  <section id="main" class="main relative">
      <div class="container">
          <div class="row justify-content-center">
              <div class="col-lg-6">
                  <div class="line-block">
                      <h2 class="lined"><?php the_title(); ?></h2>
                      <p class="lead">
                          <?php the_content() ?>
                      </p>
                  </div>
              </div>
          </div>
      </div>
  </section>
<?php endwhile; ?>

<div class="spacer-2"></div>
<?php get_footer(); ?>


<?php /* Template Name: Adicionales confirmacion */ ?>
<?php get_header(); ?>
<section id="main" class="relative main single">

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-6 text-center">
                <h1>¡Genial!</h1>
                <div class="line-block">
                    <h2 class="lined">Ya casi terminamos</h2>
                    <p class="lead">Ingresá tus datos personales para que podamos comunicarnos con vos.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="spacer-1"></div>
</section>
<section class="form-datos">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <form id="form-send" action="<?php echo home_url(); ?>/adicionales-send/" method="post" validate="true">
                    <?php
                    $modelo = $_POST['modelo_casa'];
                    $ambientes = $_POST['ambientes'];
                    $var = $_POST['options'];
                    $results = str_replace('\\', '', $var);
                    $grupos = $_POST['grupos'];
                    $grupos = str_replace('\\', '', $grupos);
                    $json = json_decode($grupos, true);
                    $json_results = json_decode($results, true);
                    ?>
                    <div class="form-group d-none">
                        <label for="nombre">Adicionales</label>
                        <textarea name="adicionales"><?php echo $results; ?></textarea>
                        <textarea name="adicionales-grupos"><?php echo $grupos; ?></textarea>
                    </div>
                    <input type="hidden" name="modelo_casa" value="<?php echo $modelo; ?>">
                    <input type="hidden" name="ambientes" value="<?php echo $_POST['ambientes'] ?>">
                    <input type="hidden" name="precio" value="<?php echo $_POST['precio'] ?>">
                    <div class="form-group">
                        <label for="nombre_usuario">Nombre</label>
                        <input type="text" class="form-control" id="nombre_usuario" name="nombre_usuario" required>
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="email" class="form-control" id="email" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="telefono">Teléfono</label>
                        <input type="mobile" class="form-control" name="telefono" id="telefono" required>
                        <small class="form-text text-muted" style="margin-top: -15px;">Con código de área y sin 0. Ej: 2235110889</small>
                    </div>
                    <div class="form-group">
                        <label for="email">Localidad</label>
                        <input type="text" name="localidad" class="form-control" id="localidad" required>
                    </div>
                    <div class="form-group">
                        <label for="comentarios">Comentarios</label>
                        <textarea class="form-control" id="mensaje" name="mensaje" rows="3" required></textarea>
                    </div>
                    <div class="text-center">
                        <input type="submit" class="btn btn-warning px-5" value="enviar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<div class="spacer-2"></div>
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="card">
                    <div class="card-body">
                        <p><strong>ELEGISTE</strong></p>
                        <h3 class="py-0 my-0">
                            <small><?php echo $modelo; ?></small>
                        </h3>
                        <H4>
                            <small><?php echo $ambientes; ?> ambiente<?php if ($ambientes>1){echo 's';} ?></small>
                        </H4>
                        <p class="lead text-primary"><strong>$ <?php echo $_POST['precio']; ?></strong></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 mt-4 mt-md-0">
                <div class="card">
                    <div class="card-body">
                        <p><strong>ADICIONALES</strong></p>
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="mb-0">INTERIOR</p>
                                <?php foreach ($json_results as $key => $value) {
                                    if (strpos($key, 'int') !== false) {
                                        foreach ($value as $v) { ?>
                                            <div class="cbox-disabled">
                                            <i class="fas fa-check icon"></i>
                                            <?php if ($v['name'] == '') { ?>
                                                <label><?php echo ''; ?></label>
                                            <?php } else { ?>
                                                <label><?php echo $v['name']; ?></label>
                                                <span></span>
                                                </div>
                                            <?php }
                                        }
                                    } ?>
                                <?php } ?>
                                <?php foreach ($json as $key => $value) {
                                    if (strpos($key, 'int') !== false) {
                                        foreach ($value as $v) { ?>
                                            <div class="cbox-disabled">
                                            <i class="fas fa-check icon"></i>
                                            <?php if ($v['item'] == '') { ?>
                                                <label><?php echo ''; ?></label>
                                            <?php } else { ?>
                                                <label><?php echo $v['grupo'] . ' ' . $v['item']; ?></label>
                                                <span></span>
                                                </div>
                                            <?php }
                                        }
                                    } ?>
                                <?php } ?>
                                <?php if ($v==""){ echo '<small class=""mt-1>No agregaste adicionales</small>';} ?>

                            </div>
                            <div class="col-lg-6">
                                <p class="mb-0">EXTERIOR</p>
                                <?php $v=""; ?>
                                <?php foreach ($json_results as $key => $value) {
                                    if (strpos($key, 'ext') !== false) {
                                        foreach ($value as $v) { ?>
                                            <div class="cbox-disabled">
                                            <i class="fas fa-check icon"></i>
                                            <?php if ($v['name'] == '') { ?>
                                                <label><?php echo ''; ?></label>
                                            <?php } else { ?>
                                                <label><?php echo $v['name']; ?></label>
                                                <span></span>
                                                </div>
                                            <?php }
                                        }
                                    } ?>
                                <?php } ?>
                                <?php foreach ($json as $key => $value) {
                                    if (strpos($key, 'ext') !== false) {
                                        foreach ($value as $v) { ?>
                                            <div class="cbox-disabled">
                                            <i class="fas fa-check icon"></i>
                                            <?php if ($v['item'] == '') { ?>
                                                <label><?php echo ''; ?></label>
                                            <?php } else { ?>
                                                <label><?php echo $v['grupo'] . ' ' . $v['item']; ?></label>
                                                <span></span>
                                                </div>
                                            <?php }
                                        }
                                    } ?>
                                <?php } ?>
                                <?php if ($v==""){ echo '<small class=""mt-1>No agregaste adicionales</small>';} ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="spacer-2"></div>
</section>


<?php get_footer(); ?>

<?php /* Template Name: Blog */ ?>

<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
    <div class="spacer-2"></div>

  <section id="main" class="main relative">
      <div class="container">
          <div class="row justify-content-center">
              <div class="col-lg-6">
                  <div class="line-block">
                      <h2 class="lined"><?php the_title(); ?></h2>
                      <p class="lead">
                          <?php the_content() ?>
                      </p>
                  </div>
              </div>
          </div>
      </div>
  </section>
<?php endwhile; ?>

<div class="spacer-2"></div>
<section id="posts" class="relative">
    <div class="container">
        <div class="row">
            <?php
            $args = array(
                'post_type'      => 'post',
                'posts_per_page' => 6,
                'order'          => 'ASC',
                'orderby'        => 'menu_order'
            );
            ?>
            <?php query_posts($args); ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part('inc/card','single'); ?>
            <?php endwhile; ?>
        </div>
        <?php echo do_shortcode('[ajax_load_more post_type="post" posts_per_page="3" offset="6" progress_bar="true" progress_bar_color="483A7A" images_loaded="true"]'); ?>
    </div>
</section>
<?php get_footer(); ?>


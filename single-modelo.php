<?php get_header(); ?>
<?php
$order_posts = new WP_Query(array(
    'post_type' => 'modelo',
    'post_status' => 'publish',
    'orderby' => 'menu_order',
    'order' => 'DESC',
));
?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="base-price hidden" id="base-price" data-base-price="<?php the_field('precio_de_base'); ?>"></div>
    <div class="header-single bg-white navbar">
        <div class="container py-4 py-lg-3">
            <div class="float-left">
                <h5 class="my-0">
                    <strong>
                        <?php if (!is_parent()) {
                            $parent = wp_get_post_parent_id(get_the_id());
                            echo get_the_title($parent);
                        } else {
                            the_title();
                        } ?>
                    </strong>
                    <span class="d-none d-md-inline-block"> | <?php the_field('ambientes'); ?> <?php if (get_field('ambientes') > 1) {
                            echo 'ambientes';
                        } else {
                            echo 'ambiente';
                        } ?></span></h5>
            </div>
            <div class="float-right">
                <h5 class="text-primary d-inline-block my-0 pr-3 align-middle" id="price-container">$<span
                            class="price-change"><?php echo number_format(get_field('precio_de_base'), 0, '', '.'); ?></span></h5>
                <input type="submit" id="submit3" class="btn btn-sm btn-warning my-0 d-none d-md-inline-block header submit-form"
                       value="ENVIAR CONSULTA">
                <!--<a href="<?php echo home_url(); ?>/adicionales-confirmacion/" id="submit3" class="btn btn-sm btn-warning my-0 d-none d-md-inline-block header">ENVIAR CONSULTA</a>-->
            </div>
        </div>
    </div>
    <section id="main" class="relative main single">
        <div class="owl-theme slider-single">
            <?php
            $images = get_field('galeria');
            $size = 'large';
            $size_md = 'medium'; // (thumbnail, medium, large, full or custom size)
            $size_xs = 'thumbnail';
            if ($images): ?>
                <?php foreach ($images as $image): ?>
                    <div class="item">
                        <div class="image-cover d-lg-block"
                             style="background-image: url(' <?php echo wp_get_attachment_url($image['ID'], $size); ?>')"></div>
                        <div class="image-cover d-none d-md-block"
                             style="background-image: url(' <?php echo wp_get_attachment_url($image['ID'], $size_md); ?>')"></div>
                        <div class="image-cover d-none d-xs-block"
                             style="background-image: url(' <?php echo wp_get_attachment_url($image['ID'], $size_xs); ?>')"></div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="clearfix"></div>
    </section>
    <section id="description" class="bg-primary text-white">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 text-center text-lg-left">
                    <div class="spacer-2"></div>
                    <h1><?php if (!is_parent()){
                            $parent= wp_get_post_parent_id(get_the_id());
                            echo get_the_title($parent);
                        } else {
                            echo get_the_title(get_the_id());
                        } ?></h1>
                    <p class="lead"><?php the_field('bajada'); ?></p>
                    <?php the_content(); ?>
                    <a href="#" class="btn btn-warning">PERSONALIZALA</a>
                    <div class="spacer-2"></div>
                </div>
                <div class="col-lg-7 align-self-end">
                    <img src="<?php the_field('personas'); ?>"
                         class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>
    <section id="personalizar" class="bg-light section ">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="line-block">
                        <h2 class="lined">Personalizá tu casa</h2>
                        <p class="lead">
                            Elegí el modelo que mejor se adapta a tu familia y personalizalo. ¡Podés elegir las terminaciones y agregar muchas cosas más para que sea única!
                        </p>
                    </div>
                </div>
            </div>
            <div class="spacer-2" id="anchor-ambientes"></div>
            <div class="row" id="ambientes">
                <div class="col-12">
                    <h4 class="text-center">Cantidad de ambientes</h4>
                </div>
                <div class="col-12">


                    <?php include('inc/single-tabs.php'); ?>


                    <div class="tab-content card" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="2-amb" role="tabpanel"
                             aria-labelledby="nav-home-tab">
                            <div class="row">
                                <div class="col-lg-4">
                                    <p><strong>CARACTERÍSTICAS</strong></p>
                                    <?php if (have_rows('caracteristicas')):
                                        while (have_rows('caracteristicas')) : the_row(); ?>
                                            <p><strong class="pr-1"><?php the_sub_field('car_nombre'); ?>
                                                    : </strong> <?php the_sub_field('car_valor'); ?></p>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                    <a href="<?php echo home_url(); ?>/catalogo" class="btn text-primary px-0">ELEGIR
                                        OTRO
                                        MODELO</a>
                                </div>
                                <div class="col-lg-8">
                                    <a href="" data-toggle="modal" data-target="#plano">
                                        <!-- VER RESOLUCIONES DE IMAGENES -->
                                        <img src="<?php echo get_field('plano')['url']; ?>" class="img-fluid"
                                             alt="Plano">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacer-2"></div>

            <!-- INTERIOR -->
            <form id="pedido" action="<?php echo home_url(); ?>/adicionales-confirmacion/" method="post">
                <input type="hidden" id="options" name="options" value="">
                <input type="hidden" id="grupos" name="grupos" value="">

                <input type="hidden" name="modelo_casa" value="
                <?php
                if (!is_parent()){
                $parent= wp_get_post_parent_id(get_the_id());
                echo get_the_title($parent);
                } else {
                    echo get_the_title(get_the_id());
                } ?>">

                <input type="hidden" name="ambientes" value="<?php the_field('ambientes') ?>">
                <input type="hidden" class="price-change-input" id="precio" name="precio" value="<?php the_field('precio_de_base'); ?>">

                <div class="row" id="interior">
                    <div class="col-12">
                        <h4 class="text-center">Detalles de interior</h4>
                    </div>
                    <div class="col-12">
                        <nav class="row mt-2">
                            <div class="nav nav-tabs mx-auto text-center" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="gen-tab" data-toggle="tab" href="#gen"
                                   role="tab" aria-controls="nav-home" aria-selected="true">Generales</a>
                                <a class="nav-item nav-link " id="coc-tab" data-toggle="tab" href="#coc" role="tab"
                                   aria-controls="nav-home" aria-selected="true">Cocina</a>
                                <a class="nav-item nav-link " id="bano-tab" data-toggle="tab" href="#bano" role="tab"
                                   aria-controls="nav-home" aria-selected="true">Baño</a>
                            </div>
                        </nav>
                        <div class="tab-content card w-100" id="nav-tabContent">
                            <?php
                            $interior = array('gen', 'coc', 'bano');
                            $i = 0;
                            foreach ($interior as $val) { ?>

                                <div class="tab-pane fade <?php if ($i == 0) {
                                    echo 'show active';
                                } ?>" id="<?php echo $val; ?>" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <p><strong>INCLUIDOS</strong></p>
                                            <?php if (get_field('int_' . $val . '_incluidos')) { ?>
                                                <?php if (have_rows('int_' . $val . '_incluidos')):
                                                    while (have_rows('int_' . $val . '_incluidos')) : the_row(); ?>
                                                        <div class="cbox-disabled">
                                                            <i class="fas fa-check icon"></i>
                                                            <label><?php the_sub_field('titulo'); ?></label>
                                                            <span></span>
                                                        </div>
                                                    <?php endwhile; ?>
                                                <?php endif; ?>
                                            <?php } ?>

                                            <?php while (have_rows('int_' . $val . '_grupo')) : the_row(); ?>
                                                <?php $grupo = get_sub_field('grupo_titulo'); ?>
                                                <?php while (have_rows('group_detalle')) : the_row(); ?>
                                                    <?php if (get_sub_field('incluido')) { ?>
                                                        <div class="cbox-disabled">
                                                            <i class="fas fa-check icon"></i>
                                                            <label><?php echo $grupo . ' : ';
                                                                the_sub_field('titulo'); ?></label>
                                                        </div>
                                                    <?php } ?>
                                                <?php endwhile; ?>
                                            <?php endwhile; ?>

                                        </div>
                                        <div class="col-lg-6">
                                            <div class="spacer-1 d-block d-md-none"></div>
                                            <div class="int">
                                                <p><strong>ADICIONALES</strong></p>
                                                <?php $i = 0; ?>
                                                <?php if (have_rows('int_' . $val . '_adicionales')):
                                                    while (have_rows('int_' . $val . '_adicionales')) : the_row(); ?>
                                                        <div class="cbox">
                                                            <input id="int-<?php echo $val; ?>-<?php echo $i; ?>"
                                                                   name="int-<?php echo $val; ?>-<?php echo $i; ?>"
                                                                   type="checkbox"
                                                                   value="<?php the_sub_field('titulo'); ?>"
                                                                   class="price-changer"
                                                                   data-price="<?php the_sub_field('valor'); ?>">
                                                            <label for="int-<?php echo $val; ?>-<?php echo $i; ?>"><?php the_sub_field('titulo'); ?></label>
                                                            <span></span>
                                                            <div class="price">$<?php the_sub_field('valor'); ?></div>
                                                        </div>
                                                        <?php $i++; endwhile; ?>
                                                <?php endif; ?>

                                                <?php if (have_rows('int_' . $val . '_grupo')):
                                                    while (have_rows('int_' . $val . '_grupo')) : the_row(); ?>
                                                        <div class="group-check val<?php echo $i; ?>">
                                                            <div class="group-title">
                                                                <?php the_sub_field('grupo_titulo'); ?>
                                                            </div>
                                                            <?php while (have_rows('group_detalle')) : the_row(); ?>
                                                                <div class="cbox">
                                                                    <?php if (get_sub_field('incluido')) { ?>
                                                                        <input id="int-group-<?php echo $val; ?>-<?php echo $i; ?>"
                                                                               name="int-group-<?php echo $val; ?>-<?php echo $i; ?>"
                                                                               type="checkbox"
                                                                               value="<?php the_sub_field('titulo'); ?>"
                                                                               class="price-changer incluido chb"
                                                                               name="group[<?php echo $i; ?>]"
                                                                               data-price="<?php the_sub_field('valor'); ?>"
                                                                               checked>
                                                                    <?php } else { ?>
                                                                        <input id="int-group-<?php echo $val; ?>-<?php echo $i; ?>"
                                                                               name="int-group-<?php echo $val; ?>-<?php echo $i; ?>"
                                                                               type="checkbox"
                                                                               value="<?php the_sub_field('titulo'); ?>"
                                                                               class="price-changer chb"
                                                                               name="group[<?php echo $i; ?>]"
                                                                               data-price="<?php the_sub_field('valor'); ?>">
                                                                    <?php } ?>
                                                                    <label for="int-group-<?php echo $val; ?>-<?php echo $i; ?>"><?php the_sub_field('titulo'); ?></label>
                                                                    <span></span>
                                                                    <?php if (get_sub_field('incluido')) { ?>
                                                                        <div class="price included">INCLUIDO</div>
                                                                    <?php } else { ?>
                                                                        <div class="price">
                                                                            $<?php the_sub_field('valor'); ?></div>
                                                                    <?php } ?>
                                                                </div>
                                                                <?php $i++; endwhile; ?>
                                                        </div>
                                                    <?php endwhile; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++;
                            } ?>
                        </div>
                    </div>
                </div>

                <div class="spacer-2"></div>


                <div class="row" id="exterior">
                    <div class="col-12">
                        <h4 class="text-center">Detalles de exterior</h4>
                    </div>
                    <div class="col-12">
                        <nav class="row mt-2">
                            <div class="nav nav-tabs mx-auto text-center" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="gene-tab" data-toggle="tab" href="#gen-ext"
                                   role="tab" aria-controls="nav-home" aria-selected="true">Generales</a>
                                <a class="nav-item nav-link " id="bases-tab" data-toggle="tab" href="#base" role="tab"
                                   aria-controls="nav-home" aria-selected="true">Bases</a>
                                <a class="nav-item nav-link " id="predio-tab" data-toggle="tab" href="#predio"
                                   role="tab" aria-controls="nav-home" aria-selected="true">Predio</a>
                            </div>
                        </nav>
                        <div class="tab-content card w-100" id="nav-tabContent">
                            <?php
                            $exterior = array('gen', 'base', 'predio');
                            $i = 0;
                            foreach ($exterior as $val) { ?>

                                <div class="tab-pane fade <?php if ($i == 0) {
                                    echo 'show active';
                                } ?>" id="<?php if ($val == 'gen') {
                                    echo 'gen-ext';
                                } else {
                                    echo $val;
                                } ?>" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <p><strong>INCLUIDOS</strong></p>
                                            <?php if (get_field('ext_' . $val . '_incluidos')) { ?>
                                                <?php if (have_rows('ext_' . $val . '_incluidos')):
                                                    while (have_rows('ext_' . $val . '_incluidos')) : the_row(); ?>
                                                        <div class="cbox-disabled">
                                                            <i class="fas fa-check icon"></i>
                                                            <label><?php the_sub_field('titulo'); ?></label>
                                                            <span></span>
                                                        </div>
                                                    <?php endwhile; ?>
                                                <?php endif; ?>
                                            <?php } ?>

                                            <?php while (have_rows('ext_' . $val . '_grupo')) : the_row(); ?>
                                                <?php $grupo = get_sub_field('grupo_titulo'); ?>
                                                <?php while (have_rows('group_detalle')) : the_row(); ?>
                                                    <?php if (get_sub_field('incluido')) { ?>
                                                        <div class="cbox-disabled">
                                                            <i class="fas fa-check icon"></i>
                                                            <label><?php echo $grupo . ' : ';
                                                                the_sub_field('titulo'); ?></label>
                                                        </div>
                                                    <?php } ?>
                                                <?php endwhile; ?>
                                            <?php endwhile; ?>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="spacer-1 d-block d-md-none"></div>
                                            <div class="ext">
                                                <p><strong>ADICIONALES</strong></p>
                                                <?php $i = 0; ?>
                                                <?php if (have_rows('ext_' . $val . '_adicionales')):
                                                    while (have_rows('ext_' . $val . '_adicionales')) : the_row(); ?>
                                                        <div class="cbox">
                                                            <input id="ext-<?php echo $val; ?>-<?php echo $i; ?>"
                                                                   name="ext-<?php echo $val; ?>-<?php echo $i; ?>"
                                                                   type="checkbox"
                                                                   value="<?php the_sub_field('titulo'); ?>"
                                                                   class="price-changer"
                                                                   data-price="<?php the_sub_field('valor'); ?>">
                                                            <label for="ext-<?php echo $val; ?>-<?php echo $i; ?>"><?php the_sub_field('titulo'); ?></label>
                                                            <span></span>
                                                            <div class="price">$<?php the_sub_field('valor'); ?></div>
                                                        </div>
                                                        <?php $i++; endwhile; ?>
                                                <?php endif; ?>

                                                <?php if (have_rows('ext_' . $val . '_grupo')):
                                                    while (have_rows('ext_' . $val . '_grupo')) : the_row(); ?>
                                                        <div class="group-check val<?php echo $i; ?>">
                                                            <div class="group-title">
                                                                <?php the_sub_field('grupo_titulo'); ?>
                                                            </div>
                                                            <?php while (have_rows('group_detalle')) : the_row(); ?>
                                                                <div class="cbox">
                                                                    <?php if (get_sub_field('incluido')) { ?>
                                                                        <input id="ext-group-<?php echo $val; ?>-<?php echo $i; ?>"
                                                                               name="ext-group-<?php echo $val; ?>-<?php echo $i; ?>"
                                                                               type="checkbox"
                                                                               value="<?php the_sub_field('titulo'); ?>"
                                                                               class="price-changer incluido chb"
                                                                               name="group[<?php echo $i; ?>]"
                                                                               data-price="<?php the_sub_field('valor'); ?>"
                                                                               checked>
                                                                    <?php } else { ?>
                                                                        <input id="ext-group-<?php echo $val; ?>-<?php echo $i; ?>"
                                                                               name="ext-group-<?php echo $val; ?>-<?php echo $i; ?>"
                                                                               type="checkbox"
                                                                               value="<?php the_sub_field('titulo'); ?>"
                                                                               class="price-changer chb"
                                                                               name="group[<?php echo $i; ?>]"
                                                                               data-price="<?php the_sub_field('valor'); ?>">
                                                                    <?php } ?>
                                                                    <label for="ext-group-<?php echo $val; ?>-<?php echo $i; ?>"><?php the_sub_field('titulo'); ?></label>
                                                                    <span></span>
                                                                    <?php if (get_sub_field('incluido')) { ?>
                                                                        <div class="price included">INCLUIDO</div>
                                                                    <?php } else { ?>
                                                                        <div class="price">
                                                                            $<?php the_sub_field('valor'); ?></div>
                                                                    <?php } ?>
                                                                </div>
                                                                <?php $i++; endwhile; ?>
                                                        </div>
                                                    <?php endwhile; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php $i++;
                            } ?>
                        </div>
                    </div>
                </div>

                <div class="spacer-1"></div>
                <div class="row justify-content-center">
                    <div class="col-10 text-center">
                        <p class="lead text-center">Si ya seleccionaste todas las opciones para tu casa podés enviar el
                            formulario para que te brindemos un asesoramiento personalizado.</p>
                        <input type="submit" id="submit2" class="submit-form btn btn-lg btn-warning mx-auto mt-3"
                               value="ENVIAR CONSULTA">
                    </div>
                </div>
            </form>
            <div class="spacer-2"></div>
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <p class="lead">
                        ¿El modelo no se adapta a lo que necesitás? ¡Consultanos por modelos personalizados!
                    </p>

                    <p>
                        <a href="#" class="btn btn-outline-primary">CONSULTANOS</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="plano" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog bg-none" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <strong><?php the_title(); ?> </strong><span
                            class="d-none d-md-inline-block pl-2">  |  2 ambientes</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/casas/sauce/SAUCE_2-100.jpg"
                         class="img-fluid w-100" alt="Plano">
                </div>
            </div>
        </div>
    </div>
<?php endwhile; endif; ?>
<?php include('inc/section-icons.php'); ?>
<?php get_footer() ?>

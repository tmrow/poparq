<?php 
/**
 * Template Name: Adicionales Send
 *
 */
?>
<?php get_header(); ?>

<?php

$adicionales = $_POST['adicionales'];
$adicionales_grupos = $_POST['adicionales-grupos'];
$nombre = $_POST['nombre_usuario'];
$email = $_POST['email'];
$tlf = $_POST['telefono'];
$localidad = $_POST['localidad'];
$mensaje = $_POST['mensaje'];
$precio = $_POST['precio'];
$sent = 0;
$modelo=$_POST['modelo_casa'];
$ambientes=$_POST['ambientes'];

if ($nombre!="" and $email!="" and $localidad!="") {
    $json = json_decode(str_replace('\\', '', $adicionales), true);
    $json_grupos = json_decode(str_replace('\\', '', $adicionales_grupos), true);
    /// ADMINISTRADOR ///////
///
///
///
    $emailTo = 'ventas@poparq.com.ar';
    //$emailTo = 'dgmarcosmoreira@gmail.com';
    $subject = 'From ' . $nombre;
    $body = '';
    $body .= "<h2>Pedido de cotización</h2> \n\n\n";
    $body .= "<hr> \n\n\n";
    $body .= "Nombre: " . $nombre . "\n\n\n";
    $body .= "Telefono: " . $tlf . "\n\n\n";
    $body .= "Email: " . $email . "\n\n\n";
    $body .= "Localidad: " . $localidad . "\n\n\n";
    $body .= "Comentarios: " . $mensaje . "\n\n\n";
    $body .= "<hr> \n\n\n";
    $body .= "<h3>Adicionales seleccionados: </h3>\n\n\n\n";
    foreach ($json as $key => $value) {
        if (count($value) > 0) {
            switch ($key) {
                case 'intGen':
                    $key = 'Interior General';
                    break;
                case 'intCoc':
                    $key = 'Interior Cocina';
                    break;
                case 'intBano':
                    $key = 'Interior Baño';
                    break;
                case 'extGen':
                    $key = 'Exterior General';
                    break;
                case 'extBase':
                    $key = 'Exterior Bases';
                    break;
                case 'extPredio':
                    $key = 'Exterior Predio';
                    break;
                default:
                    # code...
                    break;
            }
            $body .= "<h4>" . $key . "</h4>\n\n\n";
        }

        foreach ($value as $v) {
            $body .= $v['name'] . "\n\n\n";
        }
    }
    $body .= 'Valor:  $' . $precio;
    $headers = 'From: ' . $name . ' <' . $emailTo . '>' . "\r\n" . 'Reply-To: ' . $email;
    wp_mail($emailTo, $subject, $body, $headers);
    $emailSent = true;


    ///// USUARIO /////

    $emailTo = $email;
    $sent = 0;
    //$emailTo = 'dgmarcosmoreira@gmail.com';
    $subject = 'From ' . $nombre;
    $body = '';
    $body .= "<h1 style='font-family: Verdana; font-size: 30px; color: #4e5cd8; font-weight: bold;'>Hola, " . $nombre . "</h1>" . "\n\n\n";
    $body .= "<h4>Gracias por comunicarte con nosotros. Recibimos tu pedido, nos pondremos en contacto lo más rápido posible. \n\n\n";
    $body .= "<hr style='margin-top: 15px;'>";
    $body .= "<h4>Tus datos</h4> \n\n\n";
    $body .= "Nombre: " . $nombre . "\n\n\n";
    $body .= "Telefono: " . $tlf . "\n\n\n";
    $body .= "Email: " . $email . "\n\n\n";
    $body .= "Localidad: " . $localidad . "\n\n\n";
    $body .= "Comentarios: " . $mensaje . "\n\n\n";
    $body .= "<h4>Adicionales seleccionados</h4>: \n\n\n\n";
    foreach ($json as $key => $value) {
        if (count($value) > 0) {
            switch ($key) {
                case 'intGen':
                    $key = 'Interior General';
                    break;
                case 'intCoc':
                    $key = 'Interior Cocina';
                    break;
                case 'intBano':
                    $key = 'Interior Baño';
                    break;
                case 'extGen':
                    $key = 'Exterior General';
                    break;
                case 'extBase':
                    $key = 'Exterior Bases';
                    break;
                case 'extPredio':
                    $key = 'Exterior Predio';
                    break;
                default:
                    # code...
                    break;
            }
            $body .= "<h4>" . $key . "</h4>\n\n\n";
        }

        foreach ($value as $v) {
            $body .= $v['name'] . "\n\n\n";
        }
    }



    $body .= 'El precio final de la cotizacion es de $' . $precio;
    $headers = 'From: ' . $name . ' <' . $emailTo . '>' . "\r\n" . 'Reply-To: ' . $email;
    wp_mail($emailTo, $subject, $body, $headers);
    $emailSent = true;

    $post_arr = array(
        'post_title' => $name . ' - ' . $email,
        'post_type' => 'pedidos',
        'post_content' => $body,
        'post_status' => 'publish',
        'post_author' => get_current_user_id());

    wp_insert_post($post_arr);

    if (isset($emailSent) && $emailSent == true) {
        $return = 'ok';

    } else {
        $return = 'no';
    }
}else{
   $return = '¿Haciendo trampa?';
}
//json_encode($return);

?>
<section id="main" class="relative main single">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-8 text-center">
                <?php if($return == 'ok'){ ?>
                    <div class="supericon ok">
                        <i class="far fa-check-circle"></i>
                    </div>
                <h1>¡Genial!</h1>
                <div class="line-block">
                    <h4 class="lined">Tu correo ha sido enviado exitosamente.</h4>
                </div>
    <?php }elseif ($return == 'no'){ ?>
                    <div class="supericon ok">
                        <i class="far fa-check-circle"></i>
                    </div>
                    <h1>¡Lo sentimos!</h1>
                    <div class="line-block">
                        <h4 class="lined">Tuvimos un problema al enviar tu información.</h4>
                    </div>
               <?php  }else{ ?>
                    <h1>Parece que hubo algún problema....</h1>
                    <div class="line-block">
                        <h4 class="lined"><small>Intenta nuevamente más tarde</small></h4>
                    </div>
                <?php } ?>
            </div>
            <div class="my-4 col-12"></div>
            <div class="col-12 text-center">
                <a href="<?php echo home_url() ?>/catalogo" class="btn btn-primary btn-lg "> COTIZAR NUEVAMENTE</a>
            </div>
            <div class="col-12 mt-3 text-center">
                <a href="<?php echo home_url() ?>" class="btn btn-link">IR AL INICIO</a>
            </div>
        </div>
    </div>
    <div class="spacer-1"></div>
</section>
<?php get_footer(); ?>
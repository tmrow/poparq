<section class="section" id="description">
    <div class="container">
        <?php if (!is_page('arquitectura-optimizada')) { ?>
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="line-block">
                        <h2 class="lined">Arquitectura optimizada</h2>
                        <p class="lead">
                            Optimizamos el proceso de contrucción tradicional para que tengas tu casa en el menor
                            tiempo, con los mejores materiales y al menor costo.
                        </p>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="row pt-3">
            <div class="col-12 col-md-6 col-lg-4">
                <div class="icon">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/home/construccion.svg"
                         alt="Construccion" width="220"/>
                </div>
                <div class="text text-center">
                    <h5>CONSTRUCCIÓN TRADICIONAL</h5>
                    <p>
                        Construimos de forma tradicional con bloques de hormigón de 20 cm. de buena calidad y larga vida
                        útil.
                    </p>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="icon">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/home/llave.svg" alt="Construccion"/>
                </div>
                <div class="text text-center">
                    <h5>LLAVE EN MANO</h5>
                    <p>
                        Te ofrecemos una solución integral entregándote tu casa terminada, equipada y lista para
                        mudarte.
                    </p>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="icon">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/home/mayor.svg" alt="Construccion"/>
                </div>
                <div class="text text-center">
                    <h5>MAYOR CONTROL</h5>
                    <p>
                        Disminuimos los tiempos y costos de producción optimizando los recursos y jornadas de trabajo.
                    </p>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 <?php if (is_home()) {
                echo 'd-none d-md-block';
            } ?>">
                <div class="icon">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/home/modelos.svg"
                         alt="Construccion"/>
                </div>
                <div class="text text-center">
                    <h5>MODELOS FLEXIBLES</h5>
                    <p>
                        Nuestros modelos no son estáticos, están pensados para diferentes posibilidades de ampliación.
                    </p>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 <?php if (is_home()) {
                echo 'd-none d-md-block';
            } ?>">
                <div class="icon">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/home/cuentas.svg"
                         alt="Construccion"/>
                </div>
                <div class="text text-center">
                    <h5>CUENTAS CLARAS</h5>
                    <p>
                        Pautamos de antemano la totalidad del presupuesto y los tiempos de construcción.
                    </p>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 <?php if (is_home()) {
                echo 'd-none d-md-block';
            } ?>">
                <div class="icon">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/home/resolvemos.svg"
                         alt="Construccion"/>
                </div>
                <div class="text text-center">
                    <h5>RESOLVEMOS TODO</h5>
                    <p>
                        Te ofrecemos el respaldo técnico y legal de nuestro equipo profesional.
                    </p>
                </div>
            </div>
        </div>
            <div class="row <?php if (!is_home()) {
                echo 'd-none d-md-block';
            }else { echo 'd-block d-md-none'; } ?>">
                <div class="col-xs-12 col-md-6 col-lg-6 d-flex justify-content-center">
                    <a class="btn btn-link" href="<?php echo home_url() ?>/arquitectura-optimizada">
                        VER MÁS <i class="fas fa-chevron-down"></i>
                    </a>
                </div>
            </div>
            <div class="pt-5 text-center">
                <p class="lead">
                    ¿Tenés alguna duda sobre nuestro proceso de construcción?
                </p>
                <p>
                    <a class="btn btn-outline-dark" href="<?php echo home_url(); ?>/contacto">CONSULTANOS</a>
                </p>
            </div>
        </div> <!-- /container -->
</section>
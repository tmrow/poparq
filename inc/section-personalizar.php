<section id="personalizar" class="bg-light section ">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-6">
        <div class="line-block">
          <h2 class="lined">Personalizá tu casa</h2>
          <p class="lead">
            Elegí el modelo que mejor se adapta a tu familia y personalizalo. ¡Podés elegir las terminaciones y agregar muchas cosas más para que sea única!
          </p>
        </div>
      </div>
    </div>
    <div class="spacer-1"></div>
    <!-- VISIBLE MD AND UP -->
    <div class="d-none d-lg-block">
      <div class="row justify-content-center">
        <?php include('cards-personalizar.php'); ?>
      </div>
    </div>
    <!-- VISIBLE XS -->
    <div class="d-block d-lg-none relative">
      <div class="owl-carousel owl-theme personalizar slider-xs">
          <?php include('cards-personalizar.php'); ?>
      </div>
      <div class="clearfix"></div>
    </div>

  </div>
</section>

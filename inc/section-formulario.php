<section id="formulario" class="<?php if (!is_page('confirmacion') or !is_page('contacto')){ echo 'bg-light'; } ?> section ">
  <div class="container">
<?php if (!is_page('confirmacion')){ ?>
      <div class="row justify-content-center">
      <div class="col-lg-6">
        <div class="line-block">
          <h2 class="lined">¿Alguna duda?</h2>
          <p class="lead">
            Completá el formulario y nos comunicamos con vos.
          </p>
        </div>
      </div>
    </div>
    <div class="spacer-1"></div>

<?php } ?>
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <form action="<?php echo home_url(); ?>/contacto-send/" method="post" validate="true">
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre_usuario" class="form-control" id="nombre_usuario" placeholder="" required>
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" name="email" class="form-control" id="email" required>
                </div>
                <div class="form-group">
                    <label for="telefono">Teléfono</label>
                    <input type="text" name="telefono" class="form-control" id="telefono" required>
                </div>
                <div class="form-group">
                    <label for="localidad">Localidad</label>
                    <input type="text" name="localidad" class="form-control" id="localidad" required>
                </div>
                <div class="form-group">
                    <label for="comentarios">Comentarios</label>
                    <textarea class="form-control" name="comentarios" id="comentarios" rows="3" required></textarea>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-warning px-5">ENVIAR</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</section>

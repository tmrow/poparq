    <div class="item col-md-4 mb-4 text-center">
      <div class="card h-100 ">
          <a href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail('medium', array('class' => 'card-img')); ?>
          </a>
          <div class="card-body">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                <h4 class="mb-3 mt-0 py-0 text-center text-md-left">
                  <?php the_title(); ?>
                </h4>
            </a>
          <p><?php echo excerpt(20); ?></p>
        </div>
      </div>
    </div>

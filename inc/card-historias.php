<div class="item <?php if ($cols){echo $cols;} ?>">
  <div class="card">
    <div class="card-body">
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <h4 class="mb-0 mt-0 py-0 text-center text-md-left">
              <strong><?php the_title(); ?></strong>
              <small><?php $rel = get_field('modelo'); echo get_the_title($rel[0]->ID); ?></small>
            </h4>
        </a>
    </div>
      <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
        <?php the_post_thumbnail('medium', array('class' => 'card-img rounded-0')); ?>
      </a>

    <div class="card-body text-center text-md-left">
      <p><?php the_field('resumen'); ?>
     </p>
    </div>
  </div>
</div>

<div class="col-12 col-lg-6 mt-4 product">
  <a href="<?php the_permalink(); ?>" title="titulo">
    <div class="card">
      <img class="card-img-top" src="<?php echo get_template_directory_uri() ?>/assets/img/camara-8.jpg" alt="">
      <div class="card-body">
        <h5><strong><?php the_title() ?></strong></h5>
        <div class="row">
          <div class="col-6">
              <?php the_field('ambientes'); ?>

              <?php if(is_parent()){ ?>
                  <?php
                  $args = array(
                      'post_type'      => 'modelo',
                      'posts_per_page' => -1,
                      'post_parent'    => get_the_id(),
                      'order'          => 'ASC',
                      'orderby'        => 'menu_order'
                  );
                  $parent = new WP_Query( $args );
                  if ( $parent->have_posts() ) : ?>
                      <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
                        <?php echo ' | '; the_field('ambientes'); ?>
                      <?php endwhile; endif; ?>
                  <?php wp_reset_postdata() ?>
              <?php } ?>
              ambientes
          </div>
          <div class="col-6 text-right">
              <?php setlocale(LC_MONETARY, 'en_US');
              $base_price=number_format(get_field('precio_de_base'), 0, '.', '.'); ?>
            desde <span class="lead text-primary"><strong>$<?php echo $base_price; ?></strong></span>
          </div>
        </div>
      </div>
    </div>
  </a>
</div>

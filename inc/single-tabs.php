<nav class="row mt-2">
    <div class="nav nav-tabs mx-auto text-center" id="nav-tab" role="tablist">
        <?php $this_id=get_the_id(); ?>
        <?php if (is_parent()): ?>
            <a class="nav-item nav-link active" id="nav-<?php the_field('ambientes'); ?>" data-price="0" data-toggle="tab" href="#<?php the_field('ambientes'); ?>-amb" role="tab" aria-controls="nav-home" aria-selected="true">
                <?php the_field('ambientes'); ?> <?php if (get_field('ambientes')>1){echo 'ambientes';}else{echo 'ambiente';} ?>
            </a>
            <?php
            $args = array(
                'post_type'      => 'modelo',
                'posts_per_page' => -1,
                'post_parent'    => $post->ID,
                'order'          => 'ASC',
                'orderby'        => 'menu_order'
            );
            $parent = new WP_Query( $args );

            if ( $parent->have_posts() ) : ?>


                <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
                    <a class="nav-item nav-link" href="<?php the_permalink(); ?>#anchor-ambientes" id="nav-<?php the_field('ambientes', get_the_id()) ?>"><?php the_field('ambientes', get_the_id()) ?> Ambientes</a>
                <?php endwhile; endif; ?>

        <?php else: ?>
            <?php $parent= wp_get_post_parent_id(get_the_id());  ?>         
            
            <a class="nav-item nav-link <?php if($parent == 0 ){ echo "active"; } ?>" id="nav-<?php the_field('ambientes', $parent); ?>"   href="<?php echo get_the_permalink($parent) ?>#anchor-ambientes" >
                <?php the_field('ambientes', $parent); ?> <?php if (get_field('ambientes', $parent)>1){echo 'ambientes';}else{echo 'ambiente';} ?>
            </a>

            <?php
             if ($parent != 0): 
                $args = array(
                    'post_type'      => 'modelo',
                    'posts_per_page' => -1,
                    'post_parent'    => $parent,
                    'order'          => 'ASC',
                    'orderby'        => 'menu_order'
                );
                $parent1 = new WP_Query( $args );

                if ( $parent1->have_posts() ) : ?>


                <?php while ( $parent1->have_posts() ) : $parent1->the_post(); ?>
                    <a class="nav-item nav-link <?php if(get_the_id()==$this_id){echo 'active';} ?>" href="<?php the_permalink(); ?>#anchor-ambientes" id="nav-<?php the_field('ambientes', get_the_id()) ?>"><?php the_field('ambientes', get_the_id()) ?> Ambientes</a>
                <?php endwhile; endif; ?>
            <?php endif ?>
        <?php endif; ?>
        <?php wp_reset_postdata(); wp_reset_query(); ?>
    </div>
</nav>



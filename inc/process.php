<?php
require 'config.php';


$nombre= $_POST["nombre"];
$phone= $_POST["telefono"];
$localidad= $_POST["localidad"];
$email= filter_var($_POST["email"], FILTER_SANITIZE_STRING);
$mensaje= $_POST["mensaje"];

$modelo=$_POST['modelo'];
$ambientes=$_POST['ambientes'];
$precio=$_POST['precio'];


$mail->setFrom($email, $nombre);
$mail->Subject = 'Mensaje desde el sitio';

$mail->AltBody = '
Nombre y apellido: '.$nombre.'
E-mail: '.$email.'
Teléfono: '.$telefono.'
Consulta: '.$mensaje.'
';

$mail->Body = '
<html>
<head>
<title>Mensaje desde el sitio</title>
</head>
<body>
<h4 style="color:#999; font-size:20px; font-weight:normal;">Mensaje desde el sitio</h4>
<hr>
<p><b style="color:#0677C4;">Nombre y apellido: </b>'.$nombre.'</p>
<p><b style="color:#0677C4;">E-mail: </b>'.$email.'</p>
<p><b style="color:#0677C4;">Teléfono: </b>'.$phone.'</p>
<p><b style="color:#0677C4;">Localidad: </b>'.$localidad.'</p>

<p><b style="color:#0677C4;">Consulta: </b>'.$mensaje.'</p>
<br>
<hr>
<br>
<p><b style="color:#0677C4;">Modelo: </b>'.$modelo.'</p>
<p><b style="color:#0677C4;">Ambientes: </b>'.$ambientes.'</p>
<p><b style="color:#0677C4;">Precio: </b>$ '.$precio.'</p>


<p><b style="color:#0677C4;">ADICIONALES INTERIOR</b></p>
';

 foreach ($_POST as $key => $value) {
      if (strpos($key, 'int') !== false) {
        $mail->Body .= $value;
        }
  }

$mail->Body .='
    <br><p><b style="color:#0677C4;">ADICIONALES EXTERIOR</b></p>';

foreach ($_POST as $key => $value) {
    if (strpos($key, 'ext') !== false) {
        $mail->Body .= $value;
    }
}

$mail->Body .=
    '<br></body>
</html>';


if(!$mail->send()) {
  echo "no";
}else{
  echo "ok";
}
?>

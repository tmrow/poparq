var formatNumber = {
    separador: ".", // separador para los miles
    sepDecimal: ',', // separador para los decimales
    formatear:function (num){
        num +='';
        var splitStr = num.split('.');
        var splitLeft = splitStr[0];
        var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
        var regx = /(\d+)(\d{3})/;
        while (regx.test(splitLeft)) {
            splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
        }
        return this.simbol + splitLeft +splitRight;
    },
    new:function(num, simbol){
        this.simbol = simbol ||'';
        return this.formatear(num);
    }
}

function full_height(){
    var ancho = jQuery(window).width();
    if (ancho>767){
        var altura = jQuery(window).height();
        jQuery('.full-height').height(altura);
        jQuery('.backdrop.full-height').height(altura);
        jQuery('.slider-single .item').height(altura-135);
        jQuery('#main.home .full-height').height(altura-140);
        jQuery('#main.home .full-height').height(altura-180);
    }else{
        var altura = 500;
        jQuery('.full-height').height(altura);
        jQuery('.backdrop.full-height').height(altura);
        jQuery('.slider-single .item').height(altura-135);
        jQuery('#main.home .full-height').height(780);

    }

    jQuery('.mid-height').height(altura/2);
    $('.single-image').width(altura/2);

}
$("#botn").click(function(e) {
    //console.log('clickkkc');
    //$('#botn').off('click');
    //e.preventDefault();
});
$("#form-send").submit(function(e) {
    //console.log('submiiiittt');
    //$('#form-send').off('submit');
    //e.preventDefault();
});
$.fn.extend({
    animateCss: function(animationName, callback) {
        var animationEnd = (function(el) {
            var animations = {
                animation: 'animationend',
                OAnimation: 'oAnimationEnd',
                MozAnimation: 'mozAnimationEnd',
                WebkitAnimation: 'webkitAnimationEnd',
            };

            for (var t in animations) {
                if (el.style[t] !== undefined) {
                    return animations[t];
                }
            }
        })(document.createElement('div'));

        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);

            if (typeof callback === 'function') callback();
        });

        return this;
    },
});


jQuery( document ).ready(function($) {
    $('.ext > .group-check').each(function(index, el) {
        //console.log('index' ,index);
        //console.log('el' ,$(this).attr('class'));
        var hola = $(this).attr('class') + ' .chb';
        hola = hola.replace('group-check','');
        hola = hola.replace(' ','.');
        //console.log(hola);
        $(hola).change(function(){

            $(hola).prop('checked',false);
            $(this).prop('checked',true).click();
        });
    });
    jQuery('a').not('[role=tab]').click(function () {
        jQuery('.loading').fadeIn('slow');
        jQuery('.loading').fadeIn('slow');
    });

    $('.int > .group-check').each(function(index, el) {
        //console.log('index' ,index);
        //console.log('el' ,$(this).attr('class'));
        var hola = $(this).attr('class') + ' .chb';
        hola = hola.replace('group-check','');
        hola = hola.replace(' ','.');
        //console.log(hola);
        $(hola).change(function(){
            $(hola).prop('checked',false);
            $(this).prop('checked',true).click();
        });
    });
    //console.log('hello');
    var base_price = parseInt($('#base-price').attr('data-base-price'));
    $(".price-changer").change(function(e) {
        // in the handler, 'this' refers to the box clicked on
        var newPrice = base_price;
        $('.price-changer:checked').each(function() {
            if (!$(this).hasClass('incluido')) {
                ////console.log('priceffff ',price);
                newPrice += parseInt($(this).attr('data-price'));
                //console.log(newPrice);
            }  
            $('.price-change').text(formatNumber.new(newPrice));
            $('.price-change-input').val(formatNumber.new(newPrice));
            $('#price-container').animateCss('bounceIn');
        });
    });
    $(".submit-form").click(function(e){
        //e.preventDefault();
        var options = {intGen:[],intCoc:[],intBano:[],extGen:[],extBase:[],extPredio:[]};
        var grupos = {int:[],ext:[]};
        var campo = '';
        var campo_id = '';

        $('input[type=checkbox]:checked').each(function(index, el) {
         if (!$(this).hasClass('incluido')) {
             if ($(this).attr('id').includes('int-gen')) {
                options.intGen.push({name : $(this).attr('value') , price : $(this).attr('data-price')});
            } else if ($(this).attr('id').includes('int-coc')) {
                options.intCoc.push({name : $(this).attr('value') , price : $(this).attr('data-price')});
            } else if ($(this).attr('id').includes('int-bano')) {
                options.intBano.push({name : $(this).attr('value') , price : $(this).attr('data-price')});
            } else if ($(this).attr('id').includes('ext-gen')) {
                options.extGen.push({name : $(this).attr('value') , price : $(this).attr('data-price')});
            } else if ($(this).attr('id').includes('ext-base')) {
                options.extBase.push({name : $(this).attr('value') , price : $(this).attr('data-price')});
            } else if ($(this).attr('id').includes('ext-predio')) {
                options.extPredio.push({name : $(this).attr('value') , price : $(this).attr('data-price')});
            }
         }

        });
        $('.ext .group-check').each(function(index, el) {
            campo = $(this).find('input:checked').val();
            campo_id = $(this).find('input:checked').attr('id');

            if (campo !== undefined && !$('#'+campo_id).hasClass('incluido')) {
                grupos.ext.push({
                        grupo: $.trim($(this).find('.group-title').html()),
                        item: campo
                    });
            }
            ////console.log($(this).find('.group-title').html());
            ////console.log($(this).find('input:checked').val());
         });
        $('.int .group-check').each(function(index, el) {
            campo = $(this).find('input:checked').val();
            campo_id = $(this).find('input:checked').attr('id');

            if (campo !== undefined && !$('#'+campo_id).hasClass('incluido')) {
                    grupos.int.push({
                        grupo: $.trim($(this).find('.group-title').html()),
                        item: campo
                    });

            }
             //console.log($(this).find('.group-title').html());
             //console.log($(this).find('input:checked').val());
         });
        ////console.log('holaaa',grupos);
        $('#options').val(JSON.stringify(options));
        $('#grupos').val(JSON.stringify(grupos));
        //alert($('#options').val());
        ////console.log(JSON.stringify(options));
        //$('#pedido').submit();
        if ($('.submit-form').hasClass('header')) {
            e.preventDefault();
            $('#pedido').submit();
        }
    });
    // the selector will match all input controls of type :checkbox
    // and attach a click event handler
    

    // bind change event to select
    $('#casos_select').on('change', function () {
        var url = $(this).val(); // get selected value
        if (url) { // require a URL
            window.location = url; // redirect
        }
        return false;
    });


    var ancho =  jQuery(window).width();
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if(ancho>767){
            if (scroll >= 100) {
                $(".main-navbar").addClass("scroll");
            }else{
                $(".main-navbar").removeClass("scroll");
            }
        }else{
            if (scroll >= 100) {
                $("#buttons-menu").addClass("scroll");
            }else{
                $("#buttons-menu").removeClass("scroll");
            }
        }
    }); //missing );

    //alert(container);
    $('body').on('shown.bs.offcanvas', function () {
        var container = $('#box-size').width();
        var windows = $(window).width();
        var distance = parseInt(windows-container)/2;
        $('.navbar-offcanvas.in .inner').css('left', distance);
    });

    $('#offmenu').on('show.bs.collapse', function () {
        //$('body').addClass('opening-menu');
    });
    $('#offmenu').on('hide.bs.collapse', function () {
        //  $('body').removeClass('opening-menu');
    });



    $('.owl-carousel').owlCarousel({
        responsive : {
            // breakpoint from 0 up
            0 : {
                items:1,
                nav:false
            },
            // breakpoint from 480 up
            768 : {
                items:2,
                nav:true
            },
            // breakpoint from 768 up
            992 : {
                items:3,
                nav:true
            }
        },
        dots:true,
        navText: ["<i class='fas fa-chevron-left'></i>","<i class='fas fa-chevron-right'></i>"]
    });

    $('.slider-historias').owlCarousel({
        responsive : {
            // breakpoint from 0 up
            0 : {
                items:1,
                nav:false
            },
            // breakpoint from 480 up
            768 : {
                items:2,
                nav:true
            },
            // breakpoint from 768 up
            992 : {
                items:3,
                nav:true
            }
        },
        slideBy: 1,
        loop: false,
        stagePadding: 10,
        dots:true,
        autoWidth: false,
        center: false,
        mergeFit: true,
        margin:30,
        navText: ["<i class='fas fa-chevron-left'></i>","<i class='fas fa-chevron-right'></i>"]

    });

    $('.slider-single').owlCarousel({
        items:1,
        loop: true,
        nav:true,
        dots:true,
        center: false,
        navText: ["<i class='fas fa-chevron-left'></i>","<i class='fas fa-chevron-right'></i>"]
    });

    $('#gallery-slider').owlCarousel({
        items:3,
        nav:true,
        dots:true,
        navText: ["<i class='fas fa-chevron-left'></i>","<i class='fas fa-chevron-right'></i>"]
    });

    full_height();


});
jQuery(window).resize(function(event) {
    full_height();
});

jQuery(window).on( "load", function() {
    jQuery('.loading').fadeOut('slow');
});